/*
 * PerkUtills.h
 *
 *  Created on: Dec 19, 2015
 *      Author: anil
 */

#ifndef PERKUTILLS_H_
#define PERKUTILLS_H_

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

class PerkUtills {
public:
	PerkUtills();
	virtual ~PerkUtills();
	void static trackEvent(string event_id);
	void static perkLogin();
	void static openPerkPotrak();
	void static openPerkServey();
	void static showAdUnit(string event_id);
	void static showDisplayAdUnit(string event_id);
	void static showVideoAdUnit(string event_id);
};

#endif /* PERKUTILLS_H_ */
