/*
 * PerkUtills.cpp
 *
 *  Created on: Dec 19, 2015
 *      Author: anil
 */

#include "PerkUtills.h"
#include <jni.h>
#include <platform/android/jni/JniHelper.h>

PerkUtills::PerkUtills() {
	// TODO Auto-generated constructor stub

}

PerkUtills::~PerkUtills() {
	// TODO Auto-generated destructor stub
}

void PerkUtills::trackEvent(string event_id) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "trackEvent",
			"(Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(event_id.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

void PerkUtills::openPerkPotrak() {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openPerkPotrak", "()V")) {

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void PerkUtills::openPerkServey() {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "openPerkServey", "()V")) {

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void PerkUtills::perkLogin() {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "perkLogin", "()V")) {

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
	}
}

void PerkUtills::showAdUnit(string event_id) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "showAdUnit",
			"(Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(event_id.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

void PerkUtills::showDisplayAdUnit(string event_id) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "showDisplayAdUnit",
			"(Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(event_id.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}

void PerkUtills::showVideoAdUnit(string event_id) {
	cocos2d::JniMethodInfo methodInfo;
	if (JniHelper::getStaticMethodInfo(methodInfo,
			"org/cocos2dx/cpp/AppActivity", "showVideoAdUnit",
			"(Ljava/lang/String;)V")) {

		jstring stringArg1 = methodInfo.env->NewStringUTF(event_id.c_str());

		methodInfo.env->CallStaticVoidMethod(methodInfo.classID,
				methodInfo.methodID, stringArg1);
		methodInfo.env->DeleteLocalRef(methodInfo.classID);
		methodInfo.env->DeleteLocalRef(stringArg1);
	}
}
