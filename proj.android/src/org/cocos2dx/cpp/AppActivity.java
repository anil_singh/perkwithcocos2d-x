/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package org.cocos2dx.cpp;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.perk.perksdk.PerkListener;
import com.perk.perksdk.PerkManager;

public class AppActivity extends Cocos2dxActivity {
	final static String API_KEY = "2de2cf0943ef58da3003099bff0f52ec0b0c8cb5";
	static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		final PerkListener localPerkListener = new PerkListener() {
			@Override
			public void onPerkEvent(final String reason) {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), reason,
								Toast.LENGTH_SHORT).show();
						// trackEvent("f1b3f72be6a3fb4dbe2c2a5f1c2d584a88e07cf2");
					}
				});
			}
		};
		activity = AppActivity.this;
		PerkManager.startSession(this, API_KEY, localPerkListener);
		super.onCreate(savedInstanceState);

		// trackEvent("df134859d8e6f27e73cf4a3d8e534cc2e647c86b");
	}

	public static void trackEvent(String EVENT_ID) {
		PerkManager.trackEvent(activity, API_KEY, EVENT_ID, false, null);
	}

	public static void perkLogin() {
		PerkManager.launchLoginPage(activity);
	}

	public static void openPerkPotrak() {
		PerkManager.showPortal(activity, API_KEY);
	}

	public static void openPerkServey() {
		PerkManager.launchDataPoints(activity);
	}

	public static void showAdUnit(String eventID) {
		PerkManager.showAdUnit(eventID);
	}

	public static void showDisplayAdUnit(String eventID) {
		PerkManager.showDisplayAdUnit(eventID);
	}

	public static void showVideoAdUnit(String eventID) {
		PerkManager.showVideoAdUnit(eventID);
	}
}
